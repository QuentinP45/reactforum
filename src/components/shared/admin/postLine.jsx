import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { API } from '../../constants.js';

import UpdateModal from '../UpdateModal';

import EditDelete from '../utils/editDelete';
import DateFormat from '../utils/dateFormat';

import StateIcon from '../stateIcon';
import UserLink from '../utils/userLink';

/**
 * Composant qui créé les lignes du tableau de post dans la page Admin
 * Enfant de AdminPost, Parent de UpdateModal, EditDelete, DateFormat,
 * StateIncon et UserLink
 * Props requis: postInfos (Object contenant les infos du post),updateList (fonction parente refresh des données)
 *
 * @class PostLine
 * @extends {Component}
 */
class PostLine extends Component {
  /**
   * Creates an instance of PostLine.
   * @param {postInfos, updateList} props
   * @memberof PostLine
   */
  constructor(props) {
    super(props);
    this.state = { 
      totalRes: 0,
      lastResDate: null,
      lastResAuthor: null,
      modalShowToggle: false
      
    }
    this.editPost = this.editPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.modalPopUpHandler = this.modalPopUpHandler.bind(this);
  }


  /**
   * Fonction changement de l'etat de la modal d'update
   *
   * @memberof PostLine
   */
  modalPopUpHandler=()=>{
    this.setState({
        modalShowToggle: !this.state.modalShowToggle
    })
  }

  editPost(){
    console.log("edit")
  }

  /**
   * Fonction qui delete le post dans l'api
   *
   * @memberof PostLine
   */
  deletePost(){
    const id = this.props.postInfos.id;
    const requestOptions = {
        method: 'DELETE',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' }
    };
    fetch(`${API}/post/${id}`, requestOptions)
    .then()
      .then(res => {
        this.props.updateList()
      })        
      .catch(console.log)
  }

  render(){
    let infos = this.props.postInfos;
    let cutTitle;
    if(infos.title.length > 55) {
      cutTitle = infos.title.substring(0, 53);
      cutTitle= cutTitle+"..."
     }else{
       cutTitle = infos.title
     }
    return (
      <tr>
        <td><StateIcon size="30"color={infos.state.icon}/></td>
        <td><Link to={`/post/${infos.id}/${infos.slug}`}>{cutTitle}</Link></td>
        <td><UserLink author={infos.author}/></td>
        <td className="d-none d-md-table-cell"><DateFormat date={infos.createdAt}/></td>
        <td>                  
          <EditDelete modalUpdate={this.modalPopUpHandler} type="post" edit={this.editPost} delete={this.deletePost}/>
          <UpdateModal show={this.state.modalShowToggle} modalUpdate={this.modalPopUpHandler} updateList={this.props.updateList} type="post" postInfos={infos}></UpdateModal>
        </td>
      </tr>      
    );
  
  }
}
 
export default PostLine;