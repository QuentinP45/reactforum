import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { API } from '../../constants.js';

import { RiDeleteBin4Fill } from "react-icons/ri";

/**
 * Composant qui créé les lignes pour le tableau user de la page Admin
 * Enfant de adminUsers
 * Props requis: user(Object contenant les infos du user), 
 * updateList(fonction parente permettant l'update des données du parent)
 *
 * @class UserLine
 * @extends {Component}
 */
class UserLine extends Component {
  /**
   * Creates an instance of UserLine.
   * @param {user, updateList} props
   * @memberof UserLine
   */
  constructor(props) {
    super(props);
    this.state = { 
      id: this.props.user.id
    }
    this.deleteUser = this.deleteUser.bind(this);
  }

  /**
   * Fonction supprimant l'utilsateur dans l'api
   *
   * @memberof UserLine
   */
  deleteUser(){
    const id = this.state.id;
    const requestOptions = {
        method: 'DELETE',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' }
    };
    fetch(`${API}/user/${id}`, requestOptions)
    .then()
      .then(res => {
        this.props.updateList()
      })        
      .catch(console.log)
  }

  render(){
    let infos = this.props.user;
    let alertText = `Etes vous sur de vouloir supprimer l'utilisateur "${infos.username}" et les données associés?`
    return (
      <tr>
        <td><Link to={`/user/${infos.id}/${infos.username}`}><h5>{infos.username}</h5></Link></td>
        <td> 
          {/* Si le role est Admin alors on ne peut pas le supprimer */}
          {infos.roles.includes("ROLE_ADMIN") ?
            <h6 className="text-right">Est Admin</h6>
          :
            <button type="button" className="close" onClick={() => {if(window.confirm(`${alertText}\r\nCette action est irréversible`)){this.deleteUser()};}}>
              <RiDeleteBin4Fill/>
            </button>
          }                  

        </td>
      </tr>      
    );
  
  }
}
 
export default UserLine;