import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import { API } from '../../constants.js';

import { RiDeleteBin4Fill } from "react-icons/ri";

/**
 * Composant qui créé les lignes pour le tableau user de la page Admin
 * Enfant de adminCategories
 * Props requis: show (fonction parent affichant ou non la modal d'ajout)
 * modalUpdate (fonction changeant le state de la modal)
 * updateList (fonction de refresh des données en cas de changement)
 *
 * @class CategoryLine
 * @extends {Component}
 */
class CategoryLine extends Component {
  /**
   * Creates an instance of CategoryLine.
   * @param {show, modalUpdate, updateList} props
   * @memberof CategoryLine
   */
  constructor(props) {
    super(props);
    this.state = { 
      id: this.props.category.id
    }
    this.deleteCategory = this.deleteCategory.bind(this);
  }

  /**
   * Fonction de suppression de la catégorie dans l'api
   *
   * @memberof CategoryLine
   */
  deleteCategory(){
    const id = this.state.id;
    const requestOptions = {
        method: 'DELETE',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' }
    };
    fetch(`${API}/category/${id}`, requestOptions)
    .then()
      .then(res => {
        this.props.updateList()
      })        
      .catch(console.log)
  }

  render(){
    let infos = this.props.category;
    let alertText = "Etes vous sur de vouloir supprimer cette catégorie et les posts associés?"
    return (
      <tr>
        <td><Link to={`/category/${infos.id}`}>{infos.title}</Link></td>
        <td>                  
          <button type="button" className="close" onClick={() => {if(window.confirm(`${alertText}\r\nCette action est irréversible`)){this.deleteCategory()};}}>
            <RiDeleteBin4Fill/>
          </button>
        </td>
      </tr>      
    );
  
  }
}
 
export default CategoryLine;