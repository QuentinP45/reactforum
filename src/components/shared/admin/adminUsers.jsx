import React, { Component, Fragment } from 'react';
import { API } from '../../constants.js';
import UserLine from './userLine';
import Table from 'react-bootstrap/Table';

/**
 * Composant affichant le tableau des users sur la page Admin
 * Enfant de UserInfos, Parent de UserLine
 * Props requis: aucun
 *
 * @class AdminUsers
 * @extends {Component}
 */
class AdminUsers extends Component {
    
    state = {
        users: []
    }

    /**
     * Fonction qui fetch tous les users du forum
     *
     * @memberof AdminUsers
     */
    getAllUsers(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'include'
        };
        fetch(`${API}/users`, requestOptions)
        .then(res => res.json())
          .then(res => {
            this.setState({ 
                users: res['hydra:member'],
            })
        })
    }

    /**
     * Au didMount du composant, appel a la fonction de fetch
     *
     * @memberof AdminUsers
     */
    componentDidMount(){
        this.getAllUsers();
    }


    render(){
        return (
            <Fragment >
                <Table className="bg-light" striped bordered hover size="sm">
                    <thead>
                        <tr>
                            <th>Nom</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.users.map((user,i , arr) => {
                            return(
                                <UserLine key={user.id} user={user} updateList={() => this.getAllUsers()} /> 
                            )
                        })}
                    </tbody>
                </Table>
            </Fragment>
        );
    }
}
export default AdminUsers;