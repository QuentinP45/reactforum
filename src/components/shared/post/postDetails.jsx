import React, { Component, Fragment } from 'react';
import { API } from '../../constants.js';

import Badge from 'react-bootstrap/Badge';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner'

import StateIcon from '../stateIcon';
import DateFormat from '../utils/dateFormat';
import UserLink from '../utils/userLink';
import PostResponses from './postResponses';
import NewResponseForm from '../form/newResponseForm';

/**
 * Composant affichant le post détaillé, le formulaire de réponse et les réponses
 * Enfant de Post, Parent de StateIcon, DateFormat, UserLink, PostResponse et NewResponseForm
 * Props requis: id (param url contenant l'id du post), slug (param url contenant le slug du post),
 * userInfos (infos user connecté), isAuth (boolean de connexion)
 *
 * @class PostDetails
 * @extends {Component}
 */
class PostDetails extends Component {
    
    state = {
        id: null,
        title: null,
        content:null,
        category: {},
        author :{},
        createDate: null,
        updateDate:  null,
        state: {},
        tags: [],
        response: false,
    }

    /**
     * Appel à l'api pour récuperer les infos du post
     * depuis l'id passé en url
     *
     * @memberof PostDetails
     */
    getPostDetail(){
        const idpost = this.props.id;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/post/`+idpost, requestOptions)
        .then(res => res.json())
          .then(res => {
              let postInfos = res;
            this.setState({ 
                id: postInfos.id,
                title: postInfos.title,
                content:postInfos.content,
                category: postInfos.category,
                author : postInfos.author,
                createDate: postInfos.createdAt,
                updateDate:  postInfos.updatedAt,
                state: postInfos.state,
                tags: postInfos.tags,
            })
          })
    }

    /**
     * Au lancement du composant, appel à la fonctions qui 
     * récupere les infos depuis l'api
     *
     * @memberof PostDetails
     */
    componentDidMount(){
        this.getPostDetail();
    }


    render(){
        let infosPosts = this.state;
        let slugPost = this.props.slug;
        return (
            <div className="container mt-2">
                <Jumbotron className="pb-2 pt-5">
                    <Row className="align-items-center">
                        <Col md={11} className="text-break">
                            <h4 className="mr-auto">{infosPosts.title}</h4>
                        </Col>
                        <Col md={1} className="text-center">
                            <StateIcon size="40" color={infosPosts.state.icon}/>
                        </Col>
                    </Row>
                    <hr/>
                    <Row className="alert alert-dark">
                        <h6 className="col-md-6 text-center">Catégorie: {infosPosts.category.title}</h6>
                        <h6 className="col-md-6 text-center">Par: <UserLink author={infosPosts.author}/>, <DateFormat date={infosPosts.createDate}/></h6>
                    </Row>
                    <hr/>
                    <Row>
                        <Col md={2}>
                            <h5 className="text-center">Contenu:</h5>
                        </Col>
                        <Col md={9}>
                            <p className="text-break">{infosPosts.content}</p>
                        </Col>
                    </Row>
                    {infosPosts.tags.length > 0 &&
                        <Fragment>
                        <hr/>
                        <Row className="justify-content-center align-items-baseline">
                            <h6>Tags:</h6>
                            <div>
                                {infosPosts.tags.map((tag,i , arr) => {
                                    return(
                                        <Badge className="ml-1" key={tag.id} pill variant="info">{tag.title}</Badge>
                                    )                                   
                                })}
                            </div>
                        </Row>
                        </Fragment>
                    }
                </Jumbotron>

                <Row className="justify-content-center align-items-baseline"> 
                {infosPosts.id && this.props.isAuth ?
                        <Col lg={10} xl={8}>
                            <NewResponseForm postSlug={slugPost} postId={infosPosts.id}/> 
                        </Col>
                : 
                        <Col lg={10} xl={8} className="alert alert-warning text-center">
                            <h5>Pour répondre, merci de vous connecter!</h5>
                        </Col>
                }
                </Row>
                <Row className="justify-content-center align-items-baseline mt-2">   
                    {infosPosts.id ? 
                        <PostResponses userConnect={this.props.userInfos} postId={infosPosts.id}/>
                    :
                        <Col lg={10} xl={8} className="alert alert-info text-center">
                            <h5 className="mr-1">Chargement des commentaires</h5><Spinner animation="border" variant="secondary" />
                        </Col>
                    }
                </Row>
            </div>
        );
    }
}
export default PostDetails;