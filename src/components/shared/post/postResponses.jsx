import React, { Component, Fragment } from 'react';
import { API } from '../../constants.js';
import { IoAlertCircleOutline } from 'react-icons/io5'
import Col from 'react-bootstrap/Col';
import Pagination from '../utils/pagination';
import ResponseLine from './ResponseLine';

/**
 * Composant affichant les réponses d'un post
 * Enfant de PostDetails, Parent de Pagination, ResponseLine
 * Props requis: userConnect (infos sur user connecté), postId (int id du post concerné)
 *
 * @class PostResponses
 * @extends {Component}
 */
class PostResponses extends Component {
    
    state = {
        responses: [],
    }

    /**
     * Appel à l'api pour récuperer les réponses du post grace à son id
     *
     * @param {number} [newPage=1]
     * @memberof PostResponses
     */
    getPostResponses(newPage = 1){
        const idpost = this.props.postId;
        const page = newPage;
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/posts/${idpost}/responses?page=${page}`, requestOptions)
        .then(res => res.json())
          .then(res => {
            
            let nbPages;
            let totalItems = res['hydra:totalItems'];
            if(res.hasOwnProperty(['hydra:view'])){
              const iriLastPages = res['hydra:view']['hydra:last'];
              const chars = iriLastPages.split('?page=');
              nbPages = chars[1]
            } else{
              nbPages = "1";
            }
            this.setState({ 
                responses: res['hydra:member'],
                totalItems: totalItems,
                nbPages: nbPages,
                currentPage: page
            })
          })
    }

    /**
     * Fonction permettant le reload des réponses dans
     * le parent en cas de modification/suppression
     *
     * @param {int} newPage Page actuelle
     * @memberof PostResponses
     */
    pageChange(newPage){
        this.getPostResponses(newPage);
    }

    /**
     * Au lancement du composant, appel la fonction qui fetch l'api
     *
     * @memberof PostResponses
     */
    componentDidMount(){
        this.getPostResponses();
    }

    editResponse(){
        console.log("edit")
    }

    render(){
        return (
            <Fragment>
                {this.state.totalItems === 0 ?
                    <Col lg={10} xl={8} className="alert alert-warning text-center">
                        <h5><IoAlertCircleOutline/>    Aucun commentaire pour cet article    <IoAlertCircleOutline/></h5>
                    </Col>
                :
                    <Fragment>
                        <Col lg={10} xl={12}>
                            <h5>Commentaires ({this.state.totalItems})</h5>
                            {this.state.responses.map((response,i , arr) => {
                                return(
                                    // <p>{response.author.username}</p>
                                    (this.props.userConnect.username === response.author.username || this.props.userConnect.roles.includes("ROLE_ADMIN")) ?
                                        <ResponseLine key={response.id} updateList={() => this.getPostResponses(this.state.currentPage)} postInfos={response} updateDel={true} />
                                    :
                                        <ResponseLine key={response.id} updateList={() => this.getPostResponses(this.state.currentPage)} postInfos={response} updateDel={false} />
                                        

                                )                                   
                            })}
                        </Col>
                        {this.state.nbPages > 1 && <Pagination maxPage={this.state.nbPages} currentPage={this.state.currentPage} pageChange={this.pageChange.bind(this)}/>}
                    </Fragment>
                }
                
            </Fragment>
        );
    }
}
export default PostResponses;