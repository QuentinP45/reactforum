import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import { API } from '../../constants.js';

import UpdateModal from '../UpdateModal';

import EditDelete from '../utils/editDelete';
import DateFormat from '../utils/dateFormat';

import StateIcon from '../stateIcon';
import UserLink from '../utils/userLink';
import Badge from 'react-bootstrap/Badge';
import Card from 'react-bootstrap/Card'

/**
 * Composant qui fabrique chaque "card" à l'aide des données passés en props
 * Enfant de postTableRecents, UserInfos, Parent de UpdateModal, StateIcon, UserLink,EditDelete et DateFormat
 *
 * @class TableLine
 * @extends {Component}
 */
class TableLine extends Component {
  /**
   * Creates an instance of TableLine.
   * Les props requis sont: id d'un post, infos du post, 
   * fonction de maj des données, statut pour la suppression/modification
   * @param {*} props
   * @memberof TableLine
   */
  constructor(props) {
    super(props);
    this.state = { 
      totalRes: 0,
      lastResDate: null,
      lastResAuthor: null,
      modalShowToggle: false
      
    }
    this.editPost = this.editPost.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.modalPopUpHandler = this.modalPopUpHandler.bind(this);
  }


  /**
   * Appel à l'api pour récuperer les infos sur la derniere réponse du post
   *
   * @memberof TableLine
   */
  componentDidMount(){

    const page = "1";
    const filters = "?order[createdAt]=desc&page="+page;
    const id = this.props.postInfos.id;
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'omit'
    };
    fetch(`${API}/posts/${id}/responses${filters}`, requestOptions)
    .then(res => res.json())
      .then(res => {
        const totalRes = res['hydra:totalItems'];
        let lastResDate = null;
        let lastResAuthor = null;
        if(totalRes > 0){
          lastResDate = res['hydra:member'][0].createdAt;
          lastResAuthor = res['hydra:member'][0].author;
          
        }
        
        this.setState({ 
            totalRes,
            lastResDate,
            lastResAuthor
            
          })
      })
  }

  /**
   * Permet l'affichage ou non de la modal de modification de post
   *
   * @memberof TableLine
   */
  modalPopUpHandler=()=>{
    this.setState({
        modalShowToggle: !this.state.modalShowToggle
    })
  }

  editPost(){
    console.log("edit")
  }

  /**
   * Fonction qui permet la suppression du post dans l'api
   *
   * @memberof TableLine
   */
  deletePost(){
    const id = this.props.postInfos.id;
    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
        credentials: 'include'
    };
    fetch(`${API}/post/${id}`, requestOptions)
    .then()
      .then(res => {
        this.props.updateList()
      })        
      .catch(console.log)
  }

  render(){
    let infos = this.props.postInfos;
    let lastResDate;
    let authorResponse;
    //Chargement de composants en cas de réponse sur le post
    //Permet l'affichage de la date et du user qui a répondu
    if(this.state.totalRes > 0){
      lastResDate = <DateFormat date={this.state.lastResDate}/>
      authorResponse = <UserLink author={this.state.lastResAuthor}/>; 
    }
    //Réduction du titre pour l'affichage
    let cutTitle;
    if(infos.title.length > 30) {
      cutTitle = infos.title.substring(0, 27);
      cutTitle= cutTitle+"..."
     }else{
       cutTitle = infos.title
     }
    return (
      <Fragment>
          <div className="col-md-6 col-lg-4 mt-2">
            <Card className="h-100" >
              <Card.Header>
                <div className="row align-items-center">
                  <StateIcon size="30" color={infos.state.icon}/>
                  <Link className="ml-2 mr-auto" to={`/post/${infos.id}/${infos.slug}`}>{cutTitle}</Link>
                  {this.props.updateDel &&
                    <Fragment>
                      <EditDelete modalUpdate={this.modalPopUpHandler} type="post" edit={this.editPost} delete={this.deletePost}/>
                      <UpdateModal show={this.state.modalShowToggle} modalUpdate={this.modalPopUpHandler} updateList={this.props.updateList} type="post" postInfos={infos}></UpdateModal>
                    </Fragment>
                  }
                </div>
              </Card.Header>
              <Card.Body style={{paddingBottom:'0.33rem'}}>
                <Card.Text><small>Par: <UserLink author={infos.author}/> le <DateFormat date={infos.createdAt}/></small></Card.Text>
                <Card.Text><small className="text-muted">Catégorie: {infos.category.title}</small></Card.Text>
                {this.state.totalRes === 0 && <Card.Text><small className="text-muted">0 réponse</small></Card.Text>}
                {this.state.totalRes === 1 && <Card.Text><small className="text-muted">{this.state.totalRes} réponse (dernière par {authorResponse} le {lastResDate})</small></Card.Text>}
                {this.state.totalRes > 1 && <Card.Text><small className="text-muted">{this.state.totalRes} réponses (dernière par {authorResponse} le {lastResDate})</small></Card.Text>}
              </Card.Body>
              <Card.Footer className="text-muted">
                <div className="d-flex no-wrap overflow-auto scroll-horizontal">
                  {infos.tags.length > 0 ?
                    infos.tags.map((tag,i , arr) => {
                      return(
                        <Badge className="m-1" key={tag.id} pill variant="info">{tag.title}</Badge>
                      )                                   
                    })
                  :
                    <Badge className="m-1" key="no-tag" pill variant="primary">Aucun Tag</Badge>
                  }
                </div>
              </Card.Footer>
            </Card>
          </div>
      </Fragment>
    );
  
  }
}
 
export default TableLine;