import React, { Component, Fragment } from 'react';
import {Link} from 'react-router-dom';
import { API } from '../../constants.js';
import UpdateModal from '../UpdateModal';

import DateFormat from '../utils/dateFormat';
import EditDelete from '../utils/editDelete';

import StateIcon from '../stateIcon';
import Card from 'react-bootstrap/Card'

/**
 * Composant qui fabrique chaque "card" de reponse à l'aide des données passés en props
 * Enfant de UserInfos, Parent de UpdateModal, EditDelete et DateFormat et StateIncon
 * @class TableLineResponse
 * @extends {Component}
 */
class TableLineResponse extends Component {
  /**
   * Creates an instance of TableLineResponse.
   * Les props requis sont: infos de la réponse, fonction de maj de la liste, 
   * statut pour la suppression/modification 
   * @param {*} props
   * @memberof TableLineResponse
   */
  constructor(props) {
    super(props);
    this.state = { 
      modalShowToggle: false
    }
    this.editResponse = this.editResponse.bind(this);
    this.deleteResponse = this.deleteResponse.bind(this);
    this.modalPopUpHandler = this.modalPopUpHandler.bind(this);
}

  /**
   * Permet l'affichage ou non de la modal de modification de réponse
   *
   * @memberof TableLineResponse
   */
  modalPopUpHandler=()=>{
    this.setState({
        modalShowToggle: !this.state.modalShowToggle
    })
  }

  editResponse(){
    console.log("edit")
  }

  /**
   * Fonction qui permet la suppression d'une réponse dans l'api
   *
   * @memberof TableLineResponse
   */
  deleteResponse(){
        const id = this.props.responsesInfos.id;
        const requestOptions = {
            method: 'DELETE',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'include'
        };
        fetch(`${API}/response/${id}`, requestOptions)
        .then()
          .then(res => {
            this.props.updateList()
          })        
          .catch(console.log)
  }

  render(){
    let infos = this.props.responsesInfos;
    //Réduction du contenu de la réponse pour l'affichage
    let cutContent;
    if(infos.content.length > 180) {
      cutContent = infos.content.substring(0, 177);
      cutContent= cutContent+"..."
   }else{
     cutContent = infos.content
   }
   //Réduction du titre du post associé pour l'affichage
   let cutTitle;
   if(infos.post.title.length > 30) {
     cutTitle = infos.post.title.substring(0, 27);
     cutTitle= cutTitle+"..."
    }else{
      cutTitle = infos.post.title
    }
    return (
      <Fragment>
        <div className="col-md-6 col-lg-4 mt-2">
          <Card className="h-100">
            <Card.Header>
              <div className="row align-items-center">
                <StateIcon size="30" color={infos.post.state.icon}/>
                <Link className="ml-2 mr-auto" to={`/post/${infos.post.id}/${infos.post.slug}`}>{cutTitle}</Link>
                {this.props.updateDel &&
                  <Fragment>
                    <EditDelete modalUpdate={this.modalPopUpHandler} type="response" edit={this.editResponse} delete={this.deleteResponse}/>
                    <UpdateModal show={this.state.modalShowToggle} modalUpdate={this.modalPopUpHandler} updateList={this.props.updateList} type="response" responseInfos={infos}></UpdateModal>
                  </Fragment>
                }
              </div>
            </Card.Header>
            <Card.Body style={{paddingBottom:'0.33rem'}}>
              <h6 className="card-title">Réponse:</h6>
              <p className="card-text text-justify">{cutContent}</p>
              <blockquote className="blockquote mb-0 text-right" style={{fontSize:"15px"}}>
                <p className="card-text"><small className="text-muted">Catégorie: {infos.post.category.title}</small></p>
              </blockquote>
            </Card.Body>
            <Card.Footer className="text-muted pt-1 pb-1" style={{fontSize:"12px"}}>
              Réponse du <DateFormat date={infos.createdAt}/>
            </Card.Footer>
          </Card>
        </div>
      </Fragment>
    );
  
  }
}
 
export default TableLineResponse;