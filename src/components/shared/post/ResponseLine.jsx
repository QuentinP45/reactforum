import React, { Component } from 'react';
import { API } from '../../constants.js';
import UpdateModal from '../UpdateModal';
import Card from 'react-bootstrap/Card'
import EditDelete from '../utils/editDelete';
import DateFormat from '../utils/dateFormat';

import UserLink from '../utils/userLink';

/**
 * Composant qui créé les cards de reponses pour un post
 * Enfant de PostResponses, Parent de UpdateModal, EditDelete, DateFormat et UserLink
 * Props requis: updateList (fonction parente refresh des réponses), postInfos (infos sur la réponse), 
 * updateDel (boolean permettant affhichage des icones de modification/suppression)
 *
 * @class TableLine
 * @extends {Component}
 */
class TableLine extends Component {
  /**
   * Creates an instance of TableLine.
   * @param {updateList, postInfos, updateDel} props
   * @memberof TableLine
   */
  constructor(props) {
    super(props);
    this.state = { 
      modalShowToggle: false
    }
    this.editResponse = this.editResponse.bind(this);
    this.deleteResponse = this.deleteResponse.bind(this);
    this.modalPopUpHandler = this.modalPopUpHandler.bind(this);
}


  /**
   * Permet l'affichage ou non de la modal de modification de réponse
   *
   * @memberof TableLine
   */
  modalPopUpHandler=()=>{
    this.setState({
        modalShowToggle: !this.state.modalShowToggle
    })
  }

  editResponse(){
    console.log("edit")
  }

  /**
   * Fonction qui permet la suppression de la réponse dans l'api
   *
   * @memberof TableLine
   */
  deleteResponse(){
    const id = this.props.postInfos.id;
    const requestOptions = {
        method: 'DELETE',
        credentials: 'include',
        headers: { 'Content-Type': 'application/json' }
    };
    fetch(`${API}/response/${id}`, requestOptions)
    .then()
      .then(res => {
        this.props.updateList()
      })        
      .catch(console.log)
  }

  render(){
    let response = this.props.postInfos
    return (
      <Card className="mb-1" style={{backgroundColor:'#e9ecef'}}>
        <Card.Body>
          {this.props.updateDel &&
            <EditDelete modalUpdate={this.modalPopUpHandler} type="response" edit={this.editResponse} delete={this.deleteResponse}/>
          }
          <Card.Subtitle >Par: <UserLink author={response.author}/>, le <DateFormat date={response.createdAt}/></Card.Subtitle>
          <Card.Text className="mt-2">{response.content}</Card.Text>
        </Card.Body>
        {this.props.updateDel &&
          <UpdateModal show={this.state.modalShowToggle} modalUpdate={this.modalPopUpHandler} updateList={this.props.updateList} type="response" responseInfos={response}></UpdateModal>
        }
      </Card>
    );
  
  }
}
 
export default TableLine;