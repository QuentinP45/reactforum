import React from 'react';
import {Link} from 'react-router-dom';

/**
 * Composant qui créer un lien vers la page d'info de l'utilsateur passé en props
 * A besoin de l'id et du username d'un user
 *
 * @param {id, username} props
 * @return {Component} 
 */
function UserLink(props) {

    return <Link to={`/user/${props.author.id}/${props.author.username}`}>{props.author.username}</Link>;
  }

export default UserLink