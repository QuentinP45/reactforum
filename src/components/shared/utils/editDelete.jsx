import React from 'react';
import { RiFileEditLine, RiDeleteBin4Fill } from "react-icons/ri";

/**
 * Composant graphique affichant les boutons de modification/suppression
 * pour les posts/réponses
 * Props requis: delete(fonction de suppression du parent), 
 * modalUpdate (fonction d'affichage de la modal de modification)
 * type (string "post" ou "response")
 *
 * @param {*} props
 * @return {*} 
 */
const EditDelete = (props)  => {
  let alertText = "Etes vous sur?";
  if(props.type === "post"){
    alertText = "Etes vous sur de vouloir supprimer ce post et les réponses associés?"
  }else if(props.type === "response"){
    alertText = "Etes vous sur de vouloir supprimer cette réponse?"
  }

  return (
      <div>
        <button type="button" className="close" onClick={() => {if(window.confirm(`${alertText}\r\nCette action est irréversible`)){props.delete()};}}>
          <RiDeleteBin4Fill/>
        </button>
        <button type="button" className="close" onClick={() => props.modalUpdate()}>
          <RiFileEditLine/>
        </button>
      </div>
    );
  }
  
export default EditDelete