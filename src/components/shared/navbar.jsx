import React, { Component, Fragment } from 'react';
import {NavLink, Link} from 'react-router-dom';
import {LinkContainer} from 'react-router-bootstrap';
import { APPNAME, API } from '../constants.js';
import { Redirect } from 'react-router';
import ModalLogin from './ModalLogin';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

import { GrLogout, GrUserManager } from "react-icons/gr";
/**
 * Composant cpntenant la NavBar du site
 * Enfant de App, Parent de ModalLogin
 * Props requis: login (fonction parente pour le Login), logout (fonction parente pour le logout),
 * username (username du user connecté), id (id du user connecté), loading(etat de chargement du site)
 *
 * @class Navigationbar
 * @extends {Component}
 */
class Navigationbar extends Component {
    
    state = {
        categories: [],
        redirect: false,
        redirectLink: '/search/',
    }

    /**
     * Appel à l'api pour récuperer les catégories de la dropdown
     *
     * @memberof Navigationbar
     */
    getCategories(){
        const requestOptions = {
            method: 'GET',
            headers: { 'Content-Type': 'application/json' },
            credentials: 'omit'
        };
        fetch(`${API}/categories`, requestOptions)
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    categories: data['hydra:member']
                })
            })
            .catch(console.log)
    }

    /**
     * Fonction pour handle la recherche, redirige vers /search/"critere de recherche voulu"
     *
     * @param {event} event
     * @memberof Navigationbar
     */
    handleSearch(event){
        let search = document.getElementById("search")
        if(search.value !== ""){
        this.setState({
            redirect: true,
            redirectLink: `/search/${search.value}`
        })
        search.value="";
        }

    }

    /**
     * Au lancement du composant, appel à l'api
     *
     * @memberof Navigationbar
     */
    componentDidMount(){
        this.getCategories();
    }

    render(){
        return (
            <Navbar sticky="top" animation="false" bg="dark" variant="dark" expand="lg">
                <Navbar.Brand to="/" >{APPNAME} {this.state.reload}</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse animation="false" id="basic-navbar-nav">
                    <Nav className="">
                        <Nav.Link exact={true} as={NavLink} to="/">Articles Récents</Nav.Link>
                        <Nav.Link as={NavLink} to="/createpost">Poser une question</Nav.Link>
                        <NavDropdown title="Catégories" id="basic-nav-dropdown">
                            {/* Pour chaque item dans state.categories */}
                            {this.state.categories.map((category,i , arr) => {
                                return(
                                    // On retourne un dropdown.item dans un fragment
                                    <Fragment key={category.id}>
                                        <LinkContainer to={`/category/${category.id}`}>
                                            <NavDropdown.Item >{category.title}</NavDropdown.Item>
                                        </LinkContainer>
                                        {/* Si l'item n'est pas le dernier du map alors on ajoute un divider */}
                                        {arr.length - 1 !== i && <NavDropdown.Divider />}
                                    </Fragment>
                                )                                   
                            })
                            }
 
                        </NavDropdown>


                    </Nav>
                    <Nav.Item className="ml-auto mr-5">
                        <InputGroup >
                            <FormControl  id="search" size="sm" placeholder="Recherche" />
                            <InputGroup.Append>
                                <Button size="sm" variant="secondary" onClick={(e)=>this.handleSearch(e)}>Button</Button>
                            </InputGroup.Append>
                        </InputGroup>
                    </Nav.Item>
                    {(!this.props.username && !this.props.loading) && <ModalLogin logout={this.props.logout} login={this.props.login}/>}
                    {this.props.username && 
                        <Nav.Item className="mr-1">

                            <Link className="btn btn-primary btn-sm mr-2" to={`/user/${this.props.id}/${this.props.username}`}>{this.props.username} <GrUserManager/></Link>
                            <Button size="sm" variant="danger" onClick={this.props.logout}>
                                Log Out  <GrLogout/>
                            </Button>
                        </Nav.Item>}
                </Navbar.Collapse>
                {this.state.redirect && (<Redirect to={this.state.redirectLink}/>)}
            </Navbar>
        );
    }
}
export default Navigationbar;

