import React from "react";
import ReactModalLogin from "react-modal-login";
import { API } from '../constants.js';

/**
 * Composant affichant la modal de login/register
 * Enfant de NavBar
 * Props requis: login (fonction parente de login)
 *
 * @class ModalLogin
 * @extends {React.Component}
 */
class ModalLogin extends React.Component {
  /**
   * Creates an instance of ModalLogin.
   * @param {login} props
   * @memberof ModalLogin
   */
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      loggedIn: null,
      loading: false,
      error: false,
      errorMessage: "",
      initialTab: null,
      recoverPasswordSuccess: null,
    };

  }

  /**
   * Lorsque l'utilisateur demande la connexion, appel a la fonction login du parent
   * Affiche une erreur si le formulaire n'est pas rempli ou si la requete n'aboutit pas
   *
   * @memberof ModalLogin
   */
  onLogin() {

    const username = document.querySelector('#username').value;
    const password = document.querySelector('#password').value;

    if (!username || !password) {
      this.setState({
        error: true,
        errorMessage: "Merci de remplir le formulaire entierement!"
      })
    } else {
      let connexion = async() =>{
        try{
          const res = await this.props.login(username, password)
          if(!res){
            this.setState({
              error: true,
              errorMessage: "Erreur, merci de vérifier vos données"
            })
          }
        }catch(err){
          console.log(err)
        }
      }
      connexion()
    };
  }

  /**
   * Fonction ajoutant un user dans l'api (register)
   * Appel la fonction de login en cas de succés
   *
   * @param {string} username
   * @param {string} password
   * @param {string} email
   * @return {boolean} 
   * @memberof ModalLogin
   */
  async postRegister(username, password, email) {
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username: username, email: email, password:password }),
        credentials: 'omit'
      };
      let status = await fetch(`${API}/user`, requestOptions)
      .then(res => res.json())
        .then(res => {
          this.props.login(username, password);
          return true;
        })
        .catch((error) => {
          return false; 
        })
      return(status)


  }

  /**
   * Lorsque l'utilisateur demande le register, verification du formulaire
   * Affiche une erreur si le formulaire n'est pas rempli sinon appel a postRegister()
   *
   * @memberof ModalLogin
   */
  async onRegister() {

    const login = document.querySelector('#login').value;
    const email = document.querySelector('#email').value;
    const password = document.querySelector('#password').value;
    if (!login || !email || !password || !this.isEmail(email)) {
      let message="";
      if(!this.isEmail(email)){
        message="Merci de rentrer un Email valide"
      }else{
        message="Merci de remplir le formulaire entierement!"
      }
      this.setState({
        error: true,
        errorMessage: message
      })
    } else {
      let connexion = await this.postRegister(login, password, email);
      if (!connexion){
        this.setState({
          error: true,
          errorMessage: "Impossible de vous enregistrer, merci de réessayer plus tard"
        })
      }
    }
  }

  /**
   * Fonction d'ouverture de la modal sur le tab par défaut
   *
   * @param {string} initialTab
   * @memberof ModalLogin
   */
  openModal(initialTab) {
    this.setState({
      initialTab: initialTab
    }, () => {
      this.setState({
        showModal: true,
      })
    });
  }

  /**
   * Fonction interne au fonctionnement du module react-modal-login
   * Permet de set des infos sur l'etat de connexion
   *
   * @memberof ModalLogin
   */
  afterTabsChange() {
    this.setState({
      error: null
    });
  }

  /**
   * Fonction de fermeture de la modal
   *
   * @memberof ModalLogin
   */
  closeModal() {
    this.setState({
      showModal: false,
      error: null
    });
  }

  isEmail(val) {
    let regEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!regEmail.test(val)){
      return false;
    } else{
      return true
    }
  }
  render() {

    // const isLoading = this.state.loading;

    return (
      <div>

        <button
          className="RML-btn"
          onClick={() => this.openModal('login')}
        >
          Login
        </button>

        <ReactModalLogin
          visible={this.state.showModal}
          onCloseModal={this.closeModal.bind(this)}
          initialTab={this.state.initialTab}
          error={this.state.error}
          tabs={{
            afterChange: this.afterTabsChange.bind(this),
            loginLabel: "Se connecter",
            registerLabel: "S'enregistrer"
          }}
          loginError={{
            label: this.state.errorMessage
          }}
          registerError={{
            label: this.state.errorMessage
          }}

          form={{
            onLogin: this.onLogin.bind(this),
            onRegister: this.onRegister.bind(this),
            loginBtn: {
              label: "Se Connecter"
            },
            registerBtn: {
              label: "S'enregistrer"
            },
            loginInputs: [
              {
                containerClass: 'form-group',
                label: 'Nom d\'utilisateur',
                type: 'text',
                inputClass: 'form-control',
                id: 'username',
                name: 'username',
                placeholder: 'Nom d\'utilisateur',
              },
              {
                containerClass: 'form-group',
                label: 'Mot de passe',
                type: 'password',
                inputClass: 'form-control',
                id: 'password',
                name: 'password',
                placeholder: 'Mot de passe',
              }
            ],
            registerInputs: [
              {
                containerClass: 'form-group',
                label: 'Nom d\'utilisateur',
                type: 'text',
                inputClass: 'form-control',
                id: 'login',
                name: 'login',
                placeholder: 'Nom d\'utilisateur',
              },
              {
                containerClass: 'form-group',
                label: 'Email',
                type: 'email',
                inputClass: 'RML-form-control',
                id: 'email',
                name: 'email',
                placeholder: 'Email',
              },
              {
                containerClass: 'form-group',
                label: 'Mot de passe',
                type: 'password',
                inputClass: 'form-control',
                id: 'password',
                name: 'password',
                placeholder: 'Mot de passe',
              }
            ],
          }}
        />
      </div>
    )
  }
}
export default ModalLogin;